package main

import (
	"context"
	"flag"
	"fmt"
	"gitee.com/zhucheer/pub-connect/app/client"
	vwproto "gitee.com/zhucheer/pub-connect/app/proto"
	"gitee.com/zhucheer/pub-connect/app/server"
	"google.golang.org/grpc"
)

func main() {
	workType := flag.String("work", "server", "")
	flag.Parse() // 解析参数
	fmt.Println("------->", *workType)
	cc := make(chan int, 0)

	if *workType == "server" {
		var s *server.AgentServer
		go server.StartGRPCServer(9999, s)

		select {
		case <-cc:

		}
	}

	if *workType == "client" {
		var conn *grpc.ClientConn

		opts := &client.VwPubClientOpts{
			ServerAddr:  "0.0.0.0:9999",
			AppName:     "local",
			Secret:      "111111",
			RecvHandler: recvMessageDo,
		}
		client.NewGrpcClient(context.TODO(), conn, opts)
	}

	fmt.Println("PUB-CONNECT EXIT")
}

func recvMessageDo(d *vwproto.WorkDetail) (s1 vwproto.WorkDetail, err error) {

	fmt.Println("client recv", d.AppName, d.AppUuid)

	return
}
