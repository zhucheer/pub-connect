package define

const StatusTodo = "todo"
const StatusDoing = "doing"
const StatusFailed = "fail"
const StatusSuccess = "success"

const WorkAgentCreateMsg = "CREATE_MSG"
const WorkAgentHeartbeatMsg = "HEARTBEAT_MSG"
const WorkAgentCancelMsg = "CANCEL_MSG"
const WorkActivePushMsg = "ACT_PUSH_MSG"
